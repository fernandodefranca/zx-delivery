import ApolloClient from 'apollo-boost'
import endpoints from '@app/constants/endpoints'

const client = new ApolloClient({
  uri: endpoints.graphql,
})

export default client