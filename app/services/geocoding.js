import maps from '@app/constants/maps'

function geocoding(address) {
  if (!address) return Promise.reject(new Error('Geocoding service error, address is required.'))

  return fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent(address)}&key=${maps.key}`)
    .then((response) => response.json())
    .then((res) => {
      const hasResults = res && res.results && res.results[0]
      const resultIsValid = hasResults && res.results[0].geometry.location
      if(!resultIsValid) throw new Error('Service returned no result.')
      return res.results[0].geometry.location
    })
}

export default geocoding