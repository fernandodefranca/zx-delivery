export default {
  options: {
    title: '🍺 Zé - Delivery de bebidas',
    headerStyle: {
      backgroundColor: '#000',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontSize: 14,
      fontWeight: 'bold',
    },
  }
}