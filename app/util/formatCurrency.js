const formatter = {
  format: (amount, symbol='R$') => {
    if (!amount) return '-'
    if (!amount.constructor.name==='Number') return '-'

    return symbol + ' ' + parseFloat(amount, 10).toFixed(2).split('.').join(',')
  }
}

export default formatter.format