import React from 'react'
import styled from 'styled-components/native'

const Wrapper = styled.TouchableOpacity`
  padding: 10px 10px 5px 10px;
`

const TitleText = styled.Text`
  font-size: 18px;
  font-weight: bold;
`

const Text = styled.Text`
`

const PaymentTypeText = styled.Text`
  font-size: 12px;
  color: #333;
  margin-top: 4px;
`

export default class POCListItem extends React.Component {
  handlePress = () => {
    const { id, tradingName, handlePress } = this.props
    handlePress({id, tradingName})
  }

  render() {
    const { id, tradingName, address, paymentMethods, phone } = this.props

    return (
      <Wrapper key={id} onPress={this.handlePress}>
        <TitleText>{tradingName}</TitleText>
        <Text>{address.address1}, {address.number}</Text>
        <Text>{address.city} - {address.province}</Text>
        <Text>{phone.phoneNumber}</Text>
        <PaymentTypeText>{paymentMethods}</PaymentTypeText>
      </Wrapper>
    )
  }
}
