import React from 'react'
import _get from 'lodash.get'
import styled from 'styled-components/native'

import POCListItem from './POCListItem'

const POCisAvailable = ({status}) => status === 'AVAILABLE'
const paymentIsActive = ({active}) => active

const Text = styled.Text`
  text-align: center;
  padding: 18px;
`

export default function POCList({data, mappingFn, handlePOCSelected}) {
  let items = data

  if (mappingFn) items = mappingFn(data)

  if (items.length === 0) {
    return (<Text>😬 Nenhum categoria disponível neste endereço.</Text>)
  }

  return items.map((item) => (<POCListItem {...item} handlePress={handlePOCSelected} key={item.id}/>))
}

POCList.objectMapping = (data) => {
  const pocs = _get(data, 'pocSearch', [])

  return pocs
    .filter(POCisAvailable)
    .map(({id, tradingName, address, paymentMethods, phone}) => {

      const _paymentMethods = paymentMethods
        .filter(paymentIsActive)
        .map(({title}) => title)
        .join(', ')

      return {
        id, tradingName, address, paymentMethods: _paymentMethods, phone
      }
    })
}