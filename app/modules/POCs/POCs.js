import React from 'react'
import { ScrollView } from 'react-native'
import _get from 'lodash.get'

import navigation from '@app/constants/navigation'
import BasicScreen from '@app/components/BasicScreen'
import ActivityIndicator from '@app/components/ActivityIndicator'
import QueryContainer from '@app/components/QueryContainer'
import variablesMock from '@app/mocks/queryVariablesMock'
import queries from '@app/data/queries'

import ErrorComponent from '@app/components/ErrorComponent'
import POCList from './components/POCList'

export default class POCs extends React.Component {
  static navigationOptions = {
    ...navigation.options,
    title: '🍺 Revendedores',
  }

  handlePOCSelected = ({id, tradingName}) => {
    this.props.navigation.navigate('Products', {
      pocData: {
        id,
        tradingName,
        search: '',
        categoryId: 0
      }
    })
  }

  render() {
    const queryVariables = _get(this, 'props.navigation.state.params.pocData', variablesMock)

    const queryProps = {
      query: queries.pocSearch,
      variables: queryVariables,
      errorComponent: ErrorComponent,
      loadingComponent: ActivityIndicator,
      rendererComponent: POCList,
      rendererProps: {
        handlePOCSelected: this.handlePOCSelected,
        mappingFn: POCList.objectMapping,
      },
    }

    return (
      <BasicScreen padded>
        <ScrollView>
          <QueryContainer {...queryProps} />
        </ScrollView>
      </BasicScreen>
    )
  }
}