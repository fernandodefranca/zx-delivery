import React from 'react'
import { Button, } from 'react-native'

import BasicScreen from '@app/components/BasicScreen'
import ActivityIndicator from '@app/components/ActivityIndicator'
import { Title, CallToAction, Input, ErrorText, } from './components'

export default function HomeLayout(props ) {
  const { address, isSubmitting, hasFailed, canSubmit } = props
  const { handleAddressSubmit, handleAddressChange } = props

  return (
    <BasicScreen padded>
      <Title>Bebida gelada, rápida a preço baixo</Title>
      <CallToAction>Informe o endereço de entrega:</CallToAction>
      <Input 
        underlineColorAndroid="transparent"
        returnKeyType="done"
        autoCapitalize={'none'}
        spellCheck={false}
        autoCorrect={false}
        value={address}
        editable={!isSubmitting}
        onChangeText={handleAddressChange}
        placeholder="Digite aqui" />
      {
        isSubmitting ? 
          <ActivityIndicator /> :
          <Button
            onPress={handleAddressSubmit}
            title="Buscar revendedores"
            color="#000"
            disabled={!canSubmit}
          />
      }
      {
        hasFailed && 
        <ErrorText>😬 Ocorreu um erro ao obter os dados do endereço.</ErrorText>
      }
    </BasicScreen>
  )
}
