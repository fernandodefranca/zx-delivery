import React from 'react'
import navigation from '@app/constants/navigation'
import HomeLayout from './HomeLayout'

import geocodingService from '@app/services/geocoding'

export default class Home extends React.Component {
  static navigationOptions = {
    ...navigation.options
  }

  state = {
    address: 'Rua Américo Brasiliense, São Paulo',
    isSubmitting: false,
    hasFailed: false,
  }

  navigateToPOCResults = (props) => {
    this.props.navigation.navigate('POCs', {
      pocData: {
        algorithm: 'NEAREST',
        now: new Date().toISOString(),
        ...props
      }
    })
  }

  handleAddressChange = (text) => {
    this.setState({address: text})
  }

  handleAddressSubmit = () => {
    this.setState({isSubmitting: true, hasFailed: false})
    geocodingService(this.state.address.trim())
      .then((res) => {
        const { lat, lng } = res
        this.setState({isSubmitting: false, hasFailed: false})
        this.navigateToPOCResults({lat, long:lng})
      })
      .catch((err) => {
        // TODO: Error handling service
        this.setState({isSubmitting: false, hasFailed: true})
      })
  }

  render () {
    const { handleAddressChange, handleAddressSubmit } = this
    const { address, isSubmitting, hasFailed } = this.state
    const canSubmit = !isSubmitting && address && address.length > 8

    const props = { address, isSubmitting, hasFailed, canSubmit }
    const methods = { handleAddressChange, handleAddressSubmit }

    return (
      <HomeLayout {...props} {...methods} />
    )
  }
}
