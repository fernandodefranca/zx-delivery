import 'react-native'
import React from 'react'
import Component from './HomeLayout'

import renderer from 'react-test-renderer'

it('renders correctly', () => {
  const props = {
    handleAddressSubmit: Function.prototype
  }
  const tree = renderer.create(<Component {...props} />).toJSON()
  expect(tree).toMatchSnapshot()
})
