import styled from 'styled-components/native'

export const Title = styled.Text`
  font-size: 32px;
  line-height: 34px;
  font-weight: bold;
`

export const CallToAction = styled.Text`
  margin-top: 16px;
`

export const Input = styled.TextInput`
  align-self: stretch;
  height: 36px;
  background-color: #fff;
  border-width: 1px;
  border-color: #ccc;
  margin-top: 8px;
  margin-bottom: 16px;
  border-radius: 4px;
  padding: 4px;
`

export const ErrorText = styled.Text`
  text-align: center;
  padding: 16px;
  background-color: #f44336;
  margin-top: 16px;
  color: #fff;
`