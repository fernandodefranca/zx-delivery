import 'react-native'
import React from 'react'
import { shallow } from 'enzyme'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

// ------------------------------------------------------

/**
 * Set up DOM in node.js environment for Enzyme to mount to
 */
const { JSDOM } = require('jsdom')

const jsdom = new JSDOM('<!doctype html><html><body></body></html>')
const { window } = jsdom

function copyProps(src, target) {
  Object.defineProperties(target, {
    ...Object.getOwnPropertyDescriptors(src),
    ...Object.getOwnPropertyDescriptors(target),
  })
}

global.window = window
global.document = window.document
global.navigator = {
  userAgent: 'node.js',
}
copyProps(window, global)

/**
 * Set up Enzyme to mount to DOM, simulate events,
 * and inspect the DOM in tests.
 */
configure({ adapter: new Adapter() })

/**
 * Ignore some expected warnings
 * see: https://jestjs.io/docs/en/tutorial-react.html#snapshot-testing-with-mocks-enzyme-and-react-16
 * see https://github.com/Root-App/react-native-mock-render/issues/6
 */
const originalConsoleError = console.error
console.error = (message) => {
  if (message.startsWith('Warning:')) {
    return
  }

  originalConsoleError(message)
}

// ------------------------------------------------------


import Component from './Products'

describe('Products module', () => {
  it('Does shallow rendering', () => {
    const wrapper = shallow(<Component />)
  })

  it('Has initial due state', () => {
    const wrapper = shallow(<Component />)
    const componentInstance = wrapper.instance()
    expect(wrapper.state('dueTotal')).toBe(0)
  })

  it('Calculates the sum of one product as expected', () => {
    const wrapper = shallow(<Component />)
    const componentInstance = wrapper.instance()
    componentInstance.handleAmountChange({amount:1, id:42, price:33.33})
    expect(wrapper.state('dueTotal')).toBe(33.33)
  })

  it('Calculates the sum of several products as expected', () => {
    const wrapper = shallow(<Component />)
    const componentInstance = wrapper.instance()
    componentInstance.handleAmountChange({amount:1, id:42, price:33.33})
    componentInstance.handleAmountChange({amount:1, id:99, price:12.34})
    componentInstance.handleAmountChange({amount:-1, id:42, price:33.33})
    expect(wrapper.state('dueTotal')).toBe(12.34)
  })
})
