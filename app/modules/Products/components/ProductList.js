import React from 'react'
import { FlatList, } from 'react-native'
import styled from 'styled-components/native'
import _get from 'lodash.get'

import ProductItem from './ProductItem'

const Text = styled.Text`
  text-align: center;
  padding: 18px;
`

const flatListStyle = {
  paddingLeft: 5,
  paddingRight: 5,
}

const keyExtractor = (item, index) => item.id
const numColumns = 2

export default function ProductList({data, mappingFn, handleAmountChange, cartItems}) {
  let products = data

  if (mappingFn) {
    products = mappingFn(data, handleAmountChange, cartItems)
  }

  if (products.length === 0) {
    return (<Text>😬 Nenhum produto disponível nesta categoria.</Text>)
  }

  if (products.length % numColumns !== 0) {
    products.push({empty:true})
  }

  return (
    <React.Fragment>
      <FlatList
        style={flatListStyle}
        data={products}
        keyExtractor={keyExtractor}
        numColumns={numColumns}
        renderItem={({item})=><ProductItem {...item} />}
      />
    </React.Fragment>
  )
}

ProductList.objectMapping = (data, handleAmountChange, cartItems) => {
  const products = _get(data, 'poc.products', [])
  return products
    .map(({ productVariants }, index) => {
      const { productVariantId, title, price, imageUrl } = productVariants[0]
      
      let amount = 0 
      if (cartItems[productVariantId]) {
        amount = cartItems[productVariantId].amount
      }
      
      return {
        id: productVariantId,
        name: title,
        price,
        index,
        handleAmountChange,
        amount,
        imageUrl,
      }
    })
    .filter(({price}) => {
      return !!price
    })
    .sort((a, b) => {
      const nameA = a.name
      const nameB = b.name

      if(nameA < nameB) return -1
      if(nameA > nameB) return 1
      return 0
    })
}
