import React from 'react'
import { Picker } from 'react-native-picker-dropdown'
import styled from 'styled-components/native'

const StyledWrapper = styled.View`
  border: 1px;
  width: 100%;
`

export default function CategoriesList(props) {
  const { 
    data: { allCategory },
    selectedCategoryId,
    handleCategorySelected
  } = props

  return (
    <StyledWrapper>
      <Picker
        selectedValue={selectedCategoryId}
        onValueChange={handleCategorySelected}
        mode="dialog"
      >
        <Picker.Item label="Todos produtos" value={0} />
        {
          allCategory.map(({id, title}) => 
            (<Picker.Item label={title} value={id} key={id} />)
          )
        }
      </Picker>
    </StyledWrapper>
  )
}
