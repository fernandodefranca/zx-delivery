import 'react-native'
import React from 'react'
import Component from './ProductList'

import renderer from 'react-test-renderer'

it('renders correctly an empty array', () => {
  const props = {
    data: [],
    handleAmountChange: Function.prototype,
  }
  const tree = renderer.create(<Component {...props} />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('renders correctly an array of products', () => {
  const props = {
    data: [
      {
        id: 1,
        name: "Breja",
        price: 2.4,
      },
      {
        id: 2,
        name: "Breja Weiss",
        price: 4.22,
      }
    ],
    handleAmountChange: Function.prototype,
  }
  const tree = renderer.create(<Component {...props} />).toJSON()
  expect(tree).toMatchSnapshot()
})