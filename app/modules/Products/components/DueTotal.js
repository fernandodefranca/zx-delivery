import React from 'react'
import styled from 'styled-components/native'
import formatCurrency from '@app/util/formatCurrency'

const DueAmount = styled.View`
  background-color: #000;
  width: 100%;
  padding: 8px;
`

const DueAmountText = styled.Text`
  text-align: center;
  color: #ffcc00;
  font-weight: bold;
  font-size: 22px;
`

export default class DueTotal extends React.PureComponent {  
  render() {
    const { amount } = this.props

    return (
      <DueAmount>
        <DueAmountText>
          Total: { formatCurrency(amount) }
        </DueAmountText>
      </DueAmount>
    )
  }
}
