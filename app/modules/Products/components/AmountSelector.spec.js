import 'react-native'
import React from 'react'
import Component from './AmountSelector'

import renderer from 'react-test-renderer'

it('renders correctly', () => {
  const props = {
    handleAmountChange: Function.prototype
  }
  const tree = renderer.create(<Component {...props} />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('renders correctly', () => {
  const props = {
    amount: 42,
    handleAmountChange: Function.prototype
  }
  const tree = renderer.create(<Component {...props} />).toJSON()
  expect(tree).toMatchSnapshot()
})