import 'react-native'
import React from 'react'
import Component from './ProductItem'

import renderer from 'react-test-renderer'

it('renders correctly', () => {
  const props = {
    handleAmountChange: Function.prototype
  }
  const tree = renderer.create(<Component {...props} />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('renders correctly', () => {
  const props = {
    id: 22,
    name: "Name",
    price: 32.3,
    amount: 1,
    handleAmountChange: Function.prototype
  }
  const tree = renderer.create(<Component {...props} />).toJSON()
  expect(tree).toMatchSnapshot()
})
