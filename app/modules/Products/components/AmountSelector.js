import React from 'react'
import styled from 'styled-components/native'

const Wrapper = styled.View`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: center;
`

const DecrementButton = styled.TouchableOpacity`
  background: #ccc;
  width: 36px;
  height: 36px;
  border-radius: 6px;
  align-items: center;
  justify-content: center;
`

const IncrementButton = styled(DecrementButton)`
  background-color: #fc0;
`

const ButtonText = styled.Text`
  color: #333;
  font-size: 16px;
  text-align: center;
`

const AmountWrapper = styled.View`
  flex: 1;
`

const AmountText = styled.Text`
  font-size: 18px;
  font-weight: bold;
  text-align: center;
`

export default class AmountSelector extends React.Component {
  handleIncrementAmount = () => {
    const { handleAmountChange, id, price } = this.props
    handleAmountChange && handleAmountChange({amount:1, id, price})
  }

  handleDecrementAmount = () => {
    const { handleAmountChange, id, price } = this.props
    handleAmountChange && handleAmountChange({amount:-1, id, price})
  }

  render() {
    const { amount } = this.props

    return (
      <Wrapper>
        <DecrementButton onPress={this.handleDecrementAmount}>
          <ButtonText>-</ButtonText>
        </DecrementButton>
        <AmountWrapper>
          <AmountText>{amount}</AmountText>
        </AmountWrapper>
        <IncrementButton onPress={this.handleIncrementAmount}>
          <ButtonText>+</ButtonText>
        </IncrementButton>
      </Wrapper>
    )
  }
}
