import React from 'react'
import styled from 'styled-components/native'
import AmountSelector from './AmountSelector'
import PureImage from './PureImage'

import formatCurrency from '@app/util/formatCurrency'

const Wrapper = styled.View`
  width: 50%;
  padding: 5px;
`
const Container = styled.View`
  padding: 10px;
  border: 1px;
  border-color: #ccc;
  display: flex;
  height: 256px;
  align-items: center;
  justify-content: center;
  border-radius: 4px;
`

const Name = styled.Text`
  font-size: 12px;
  line-height: 15px;
  height: 30px;
  overflow: hidden;
  font-weight: bold;
  text-align: center;
`

const Price = styled.Text`
  font-size: 22px;
  line-height: 24px;
  font-weight: bold;
  text-align: center;
  padding: 2px 0;
`

const ProductImageWrapper = styled.View`
  width: 100%;
  flex: 1;
  padding: 8px 0;
`

export default class ProductItem extends React.PureComponent {  
  render() {
    const {
      id,
      name,
      price,
      amount='',
      handleAmountChange,
      imageUrl,
    } = this.props

    if (!id) return (<Wrapper />)

    return (
      <Wrapper>
        <Container>
          <Name numberOfLines={2} ellipsizeMode="tail">{name}</Name>
          <ProductImageWrapper>
            <PureImage uri={imageUrl} id={id} />
          </ProductImageWrapper>
          <Price>{formatCurrency(price)}</Price>
          <AmountSelector
            id={id}
            price={price}
            handleAmountChange={handleAmountChange}
            amount={amount} />
        </Container>
      </Wrapper>
    )
  }
}
