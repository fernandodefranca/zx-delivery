import React from 'react'
import styled from 'styled-components/native'

const StyledImage = styled.Image`
  flex: 1;
`

export default class PureImage extends React.PureComponent {  
  render() {
    return (<StyledImage source={{ uri: this.props.uri }} />)
  }
}
