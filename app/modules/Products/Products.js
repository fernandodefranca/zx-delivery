import React from 'react'
import _get from 'lodash.get'

import navigation from '@app/constants/navigation'
import BasicScreen from '@app/components/BasicScreen'
import ActivityIndicator from '@app/components/ActivityIndicator'
import QueryContainer from '@app/components/QueryContainer'
import variablesMock from '@app/mocks/pocQueryVariablesMock'
import queries from '@app/data/queries'

import ErrorComponent from '@app/components/ErrorComponent'
import FlexView from '@app/components/FlexView'
import CategoriesList from './components/CategoriesList'
import ProductList from './components/ProductList'
import DueTotal from './components/DueTotal'

export default class Products extends React.Component {
  static navigationOptions = {
    ...navigation.options,
    title: '🍺 Produtos'
  }

  state = {
    selectedCategoryId: 0,
    cartItems: {},
    dueTotal: 0,
  }

  handleCategorySelected = (categoryId) => {
    this.setState({selectedCategoryId:categoryId})
  }

  renderCategories(queryVariables) {
    const { selectedCategoryId } = this.state
    const { handleCategorySelected } = this

    const queryProps = {
      query: queries.allCategoriesSearch,
      variables: queryVariables,
      errorComponent: ErrorComponent,
      loadingComponent: null,
      rendererComponent: CategoriesList,
      rendererProps: {
        selectedCategoryId,
        handleCategorySelected,
      },
    }

    return (
      <QueryContainer {...queryProps} />
    )
  }

  renderProducts(queryVariables) {
    const { selectedCategoryId, cartItems } = this.state

    const queryProps = {
      query: queries.pocCategorySearch,
      variables: {...queryVariables, categoryId: selectedCategoryId},
      errorComponent: ErrorComponent,
      loadingComponent: ActivityIndicator,
      rendererComponent: ProductList,
      rendererProps: {
        handleAmountChange: this.handleAmountChange,
        mappingFn: ProductList.objectMapping,
        cartItems,
      },
    }

    return (
      <QueryContainer {...queryProps} />
    )
  }

  calculateDueTotal = () => {
    const { cartItems } = this.state

    const dueTotal = Object.values(cartItems)
      .filter((item) => item.amount > 0)
      .reduce((acc, item) => {
        return acc + item.amount * item.price
      }, 0)
    
    this.setState({dueTotal})
  }

  handleAmountChange = ({amount, id, price}) => {
    const { cartItems } = this.state
    const item = cartItems[id]

    const callback = () => this.calculateDueTotal()

    if (!item) {
      if (amount > 0) {
        this.setState({cartItems: {...cartItems, [id]: {amount: 1, price}}}, callback)
      }
      return
    }

    if (amount > 0) {
      this.setState({cartItems: {
        ...cartItems, 
        [id]: {
          amount: item.amount + 1,
          price
        }
      }}, callback)
    } else {
      if (cartItems[id].amount === 0) return

      this.setState({cartItems: {
        ...cartItems, 
        [id]: {
          amount: item.amount - 1,
          price
        }
      }}, callback)
    }
  }

  render() {
    const { dueTotal } = this.state
    const hasTotal = dueTotal > 0
    const queryVariables = _get(this, 'props.navigation.state.params.pocData', variablesMock)

    return (
      <BasicScreen>
        {this.renderCategories(queryVariables)}
        <FlexView>
          {this.renderProducts(queryVariables)}
        </FlexView>
        {
          hasTotal &&
          <DueTotal amount={dueTotal} />
        }
      </BasicScreen>
    )
  }
}