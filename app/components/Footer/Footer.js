import React from 'react'
import styled from 'styled-components/native'

const StyledView = styled.View`
  background-color: #ffcc00;
  padding: 12px;
  width: 100%;
`

const StyledText = styled.Text`
  font-size: 12px;
  text-align: right;
`

export default class Footer extends React.Component {
  render() {
    const { children } = this.props
    return (
      <StyledView>
        <StyledText>{ children }</StyledText>
      </StyledView>
    )
  }
}