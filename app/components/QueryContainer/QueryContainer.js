import React from 'react'
import { Query } from 'react-apollo'

export default function QueryContainer({query, variables, errorComponent, loadingComponent, rendererComponent, rendererProps}) {
  const ErrorComponent = errorComponent
  const LoadingComponent = loadingComponent
  const RendererComponent = rendererComponent
  return (
    <Query query={query} variables={variables}>
      {
        ({loading, error, data, refetch}) => {
          if (error) {
            if (!ErrorComponent) return null
            return (<ErrorComponent retry={refetch}/>)
          }
          if (loading) {
            if (!LoadingComponent) return null
            return (<LoadingComponent />)
          }
          return (<RendererComponent data={data} {...rendererProps}/>)
        }
      }
    </Query>
  )
}