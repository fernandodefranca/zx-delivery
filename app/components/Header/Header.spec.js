import 'react-native'
import React from 'react'
import Component from './Header'

import renderer from 'react-test-renderer'

it('renders correctly', () => {
  const tree = renderer.create(<Component />).toJSON()
  expect(tree).toMatchSnapshot()
})
