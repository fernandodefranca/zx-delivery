import React from 'react'
import styled from 'styled-components/native'

const StyledView = styled.View`
  background-color: #333;
  padding: 48px 16px 16px 16px;
  width: 100%;
`

const StyledText = styled.Text`
  color: #fff;
`

export default class Header extends React.Component {
  render() {
    const { children } = this.props
    return (
      <StyledView>
        <StyledText>{ children }</StyledText>
      </StyledView>
    )
  }
}