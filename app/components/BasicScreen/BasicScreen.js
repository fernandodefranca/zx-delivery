import React from 'react'
import styled from 'styled-components/native'

import Footer from '../Footer'

const StyledView = styled.View`
  flex: 1;
  background-color: #fff;
`

const centeredStyle = {
  alignItems: 'center',
  justifyContent: 'center'
}

const paddedStyle = {
  padding: 16
}

export default class BasicScreen extends React.Component {
  render() {
    const { children, centered, padded } = this.props
    
    let contentStyle = {}
    if (centered) contentStyle = Object.assign(centeredStyle, contentStyle)
    if (padded) contentStyle = Object.assign(paddedStyle, contentStyle)

    return (
      <StyledView>
        <StyledView style={contentStyle}>
          { children }
        </StyledView>
        <Footer>2018 - Fernando de França 🚛</Footer>
      </StyledView>
    )
  }
}
