import 'react-native'
import React from 'react'
import BasicScreen from './BasicScreen'

import renderer from 'react-test-renderer'

it('renders correctly', () => {
  const tree = renderer.create(<BasicScreen />).toJSON()
  expect(tree).toMatchSnapshot()
})
