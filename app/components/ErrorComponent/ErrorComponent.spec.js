import 'react-native'
import React from 'react'
import Component from './ErrorComponent'

import renderer from 'react-test-renderer'

it('renders correctly', () => {
  const props = {}
  const tree = renderer.create(<Component {...props} />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('renders correctly', () => {
  const props = {
    message: "Erro!"
  }
  const tree = renderer.create(<Component {...props} />).toJSON()
  expect(tree).toMatchSnapshot()
})