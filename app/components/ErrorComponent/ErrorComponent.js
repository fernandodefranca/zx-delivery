import React from 'react'
import { Text, } from 'react-native'

export default function ErrorComponent({message='Um erro ocorreu'}) {
  return (<Text>{message}</Text>)
}