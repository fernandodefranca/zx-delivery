import React from 'react'
import { ActivityIndicator, } from 'react-native'

export default function StyledActivityIndicator({ size, ...props }) {
  return (
    <ActivityIndicator
      animating={true}
      hidesWhenStopped={false}
      size={size || 'large'}
      color={'#fc0'} />
  )
}
