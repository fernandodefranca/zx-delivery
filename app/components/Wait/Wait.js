import React from 'react'
import styled from 'styled-components/native'
import ActivityIndicator from '@app/components/ActivityIndicator'

const Title = styled.Text`
  font-size: 32px;
  line-height: 34px;
  font-weight: bold;
  text-align: center;
  margin-bottom: 16px;
`

const StyledView = styled.View`
  display: flex;
  justify-content: center;
  padding: 16px;
  margin: 32px;
`

export default class Wait extends React.Component {
  render() {
    return (
      <StyledView>
        <Title>Aguarde um instante</Title>
        <ActivityIndicator />
      </StyledView>
    )
  }
}
