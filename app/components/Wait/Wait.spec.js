import 'react-native'
import React from 'react'
import Component from './Wait'

import renderer from 'react-test-renderer'

it('renders correctly', () => {
  const tree = renderer.create(<Component />).toJSON()
  expect(tree).toMatchSnapshot()
})
