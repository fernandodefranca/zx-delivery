import React from 'react'
import { ApolloProvider } from 'react-apollo'
import { createStackNavigator, createAppContainer } from "react-navigation"

import graphQLClient from '@app/data/client'

import Home from '@app/modules/Home'
import POCs from '@app/modules/POCs'
import Products from '@app/modules/Products'

const AppNavigator = createStackNavigator(
  {
    Home: Home,
    POCs: POCs,
    Products: Products,
  },
  {
    initialRouteName: "Home"
  }
)

const AppNavigatorContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  render() {
    return (
      <ApolloProvider client={graphQLClient}>
        <AppNavigatorContainer />
      </ApolloProvider>
    );
  }
}
